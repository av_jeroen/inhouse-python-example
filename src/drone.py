import json
from util import get, post


class Drone:
    def __init__(self):
        pass

    def arm(self):
        """ Arming the drone will start the rotors, enabling take-off """
        return post('arm')

    def take_off(self):
        return post('takeoff')

    def go_to_waypoint(self, x, y):
        params = {
            'x': x, 'y': y
        }
        return post('waypoint', params=params)

    def return_home(self):
        return post('return_home')

    def land(self):
        return post('land')

    def get_position(self):
        return json.loads(get('location', default={}))

    def get_battery(self):
        return json.loads(get('battery', default=-1))

if __name__ == "__main__":
    d = Drone()
    pos = d.get_position()
    print(pos)