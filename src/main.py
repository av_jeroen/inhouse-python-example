from drone import Drone

import time

drone = Drone()
home = drone.get_position()
print(f"Home position: {home}")

drone.arm()
time.sleep(2)

drone.take_off()
time.sleep(5)

drone.go_to_waypoint(home['x'], home['y'] + 500)
time.sleep(20)
print(f"Current position: {drone.get_position()}")

drone.return_home()
time.sleep(20)
drone.land()

print("Finished flight")
print(f"Current battery: {drone.get_battery()}")