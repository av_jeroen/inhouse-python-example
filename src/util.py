import requests

from config import IP

def get(link, default=False):
    ''' simple helper function for quick api-get requests '''
    url = f'https://{IP}/{link}'
    result = requests.get(url)
    if result.status_code != 200:
        print(f"Get Request '{url}' went wrong. Status code {result.status_code}")
        print(f"{result.text}")
        return default
    return result.text

def post(link, params={}, default=False):
    ''' simple helper function for quick api-post requests '''
    url = f'https://{IP}/{link}'
    print(params)
    if params:
        result = requests.post(url, json=params)
    else:
        result = requests.post(url)
    if result.status_code != 200:
        print(f"Post Request '{url}' went wrong. Status code {result.status_code}")
        print(f"{result.text}")
        return default
    return result.text