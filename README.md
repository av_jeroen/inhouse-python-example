# Inhouse example code

Python example code to get started with the Avalor AI Inhouse day.

## Run

The example code will take your drone for a quick spin.

```bash
python src/main.py
```
